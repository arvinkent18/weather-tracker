const request = require('request');

let apiKey = '458f6d0227946cc3c079cffb9930d3de';
let city = 'Zamboanga';
let countryCode = 'ph';
let url = `http://api.openweathermap.org/data/2.5/weather?q=${city},${countryCode}&units=imperial&appid=${apiKey}`;

request(url, function(error, response, body) {
	if (error) {
		console.log('error:', error);
	} else {
		let weather = JSON.parse(body);
		let message = `It's ${weather.main.temp} degrees in ${weather.name}!`;
		
		console.log(message);
	}
});


